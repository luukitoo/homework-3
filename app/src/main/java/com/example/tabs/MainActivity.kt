package com.example.tabs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.tabs.adapters.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager : ViewPager2
    private lateinit var tabs : TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager = findViewById(R.id.viewPagerId)
        tabs = findViewById(R.id.tabLayoutId)

        viewPager.adapter = ViewPagerAdapter(this)
        TabLayoutMediator(tabs, viewPager) { tabs, position ->

            when (position) {
                0 -> {
                    tabs.setIcon(R.drawable.ic_baseline_calculate_24)
                    tabs.text = "Calc Design"
                }
                1 -> {
                    tabs.setIcon(R.drawable.ic_baseline_view_headline_24)
                    tabs.text = "Scroll View"
                }
                2 -> {
                    tabs.setIcon(R.drawable.ic_baseline_emoji_emotions_24)
                    tabs.text = "Meme"
                }
            }
        }.attach()

    }

}